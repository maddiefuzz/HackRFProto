//
//  ContentView.swift
//  HackRFProto
//
//  Created by maddiefuzz on 7/28/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Button(action: ExtensionManager.shared.activate) {
                Text("Activate HackRFDriver")
            }
            Button(action: ExtensionManager.shared.deactivate) {
                Text("Deactivate HackRFDriver")
            }
        }
        .frame(width: 300.0, height: 100)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
