//
//  HackRFProtoApp.swift
//  HackRFProto
//
//  Created by maddiefuzz on 7/28/22.
//

import SwiftUI

@main
struct HackRFProtoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
