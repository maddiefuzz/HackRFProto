//
//  ExtensionManager.swift
//  HackRFProto
//
//  Created by maddiefuzz on 7/31/22.
//

import Foundation
import SystemExtensions
import os.log

class ExtensionManager : NSObject, OSSystemExtensionRequestDelegate {
    
    static let shared = ExtensionManager()
    
    func activate() {
        let activationRequest = OSSystemExtensionRequest.activationRequest(forExtensionWithIdentifier: "info.maddie.HackRFProto.HackRFDriver", queue: .main)
        activationRequest.delegate = self
        OSSystemExtensionManager.shared.submitRequest(activationRequest)
    }
    
    func deactivate() {
        let activationRequest = OSSystemExtensionRequest.deactivationRequest(forExtensionWithIdentifier: "info.maddie.HackRFProto.HackRFDriver", queue: .main)
        activationRequest.delegate = self
        OSSystemExtensionManager.shared.submitRequest(activationRequest)
    }
    
    func request(_ request: OSSystemExtensionRequest, actionForReplacingExtension existing: OSSystemExtensionProperties, withExtension ext: OSSystemExtensionProperties) -> OSSystemExtensionRequest.ReplacementAction {
        return .replace
    }
    
    func requestNeedsUserApproval(_ request: OSSystemExtensionRequest) {
        os_log("sysex needsUserApproval")
    }
    
    func request(_ request: OSSystemExtensionRequest, didFinishWithResult result: OSSystemExtensionRequest.Result) {
        os_log("sysex didFinishWithResult")
    }
    
    func request(_ request: OSSystemExtensionRequest, didFailWithError error: Error) {
        os_log("sysex didFailWithError: \(error.localizedDescription)")
    }
}
