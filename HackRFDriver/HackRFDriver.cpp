//
//  HackRFDriver.cpp
//  HackRFDriver
//
//  Created by maddiefuzz on 7/28/22.
//

#include <os/log.h>

#include <DriverKit/IOUserServer.h>
#include <DriverKit/IOLib.h>
#include <USBDriverKit/IOUSBHostInterface.h>
#include <USBDriverKit/IOUSBHostPipe.h>
#include <USBDriverKit/IOUSBHostDevice.h>


#include "HackRFDriver.h"

struct HackRFDriver_IVars
{
    IOUSBHostInterface *interface;
    IOUSBHostDevice *device;
    IOUSBHostPipe *inPipe;
    OSAction *ioCompleteCallback;
    IOBufferMemoryDescriptor *inData;
    uint16_t maxPacketSize;
};

static const uint32_t kMyEndpointAddress = 1;

kern_return_t
IMPL(HackRFDriver, Start)
{
    kern_return_t ret;
    
    ret = Start(provider, SUPERDISPATCH);
    
    os_log(OS_LOG_DEFAULT, "Beginning Start function...");
    
    os_log(OS_LOG_DEFAULT, "All the start stuff went good, captain.");
    
    return ret;
}

kern_return_t
IMPL(HackRFDriver, Stop)
{
    kern_return_t ret;
    ret = Stop(provider, SUPERDISPATCH);
    os_log(OS_LOG_DEFAULT, "Goodbye World");
    return ret;
}

void HackRFDriver::free()
{
    return;
}

bool HackRFDriver::init()
{
    if(!super::init()) {
        return false;
    }
    
    ivars = IONewZero(HackRFDriver_IVars, 1);
    if(!ivars) {
        return false;
    }
    
    os_log(OS_LOG_DEFAULT, "HackRFDriver Init Function Called.");
    return true;
}

//kern_return_t HackRFDriver::CopyDescriptor(uint8_t type, uint16_t *length, uint8_t index, uint16_t languageID, uint8_t requestType, uint8_t requestRecipient, uint8_t *descriptor)
//{
//    return 0;
//}
//
//const IOUSBDeviceDescriptor * HackRFDriver::CopyDeviceDescriptor(void)
//{
//    return NULL;
//}
//
//const IOUSBStringDescriptor * HackRFDriver::CopyStringDescriptor(uint8_t index)
//{
//    return NULL;
//}
//
//const IOUSBStringDescriptor * HackRFDriver::CopyStringDescriptor(uint8_t index, uint16_t languageID)
//{
//    return NULL;
//}
//
//const IOUSBBOSDescriptor * CopyCapabilityDescriptors(void)
//{
//    return NULL;
//}

//kern_return_t
//IMPL(HackRFDriver, CopyDescriptor)
//{
//    kern_return_t ret;
//    ret = CopyDescriptor(0, 0, 0, 0, 0, 0, 0);
//    return ret;
//}
